"use strict";

const bananojs = jest.createMockFromModule("@bananocoin/bananojs");

bananojs.getAccountsPending.mockImplementation((data) => {
  if (data[0] === "ban_send30bananos") {
    return {
      blocks: {
        ban_send30bananos: {
          "4C1FEEF0BEA7F50BE35489A1233FE002B212DEA554B55B1B470D78BD8F210C74": {
            amount: "3000000000000000000000000000000",
            source:
              "ban_1hfwr3hwu3yb6pdqu4xgws3jfjbwazjhpwwfe14sz4c9u5wx4nr7excz1589",
          },
        },
      },
    };
  }
  return {
    blocks: {
      ban_1hfwr3hwu3yb6pdqu4xgws3jfjbwazjhpwwfe14sz4c9u5wx4nr7excz1589: {
        "4C1FEEF0BEA7F50BE35489A1233FE002B212DEA554B55B1B470D78BD8F210C74": {
          amount: "19000000000000000000000000000000",
          source:
            "ban_1hfwr3hwu3yb6pdqu4xgws3jfjbwazjhpwwfe14sz4c9u5wx4nr7excz1589",
        },
      },
    },
  };
});

bananojs.getAccount.mockImplementation(
  () => "ban_3zoosfctg9z54kqwwsgx1fu886xczsx1aunqdxbptbzpwbe63tuuxtffg57o"
);

module.exports = bananojs;
