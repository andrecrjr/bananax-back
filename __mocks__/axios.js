"use strict";

const axios = jest.createMockFromModule("axios");

axios.get.mockImplementation((url) => {
  switch (url) {
    case "https://api.coingecko.com/api/v3/simple/price?ids=wax&vs_currencies=usd":
      return Promise.resolve({
        data: {
          wax: {
            usd: 0.03688011,
          },
        },
      });
    case "https://api.coingecko.com/api/v3/simple/price?ids=banano&vs_currencies=usd":
      return Promise.resolve({
        data: {
          banano: {
            usd: 0.00123784,
          },
        },
      });
    default:
      return Promise.reject(new Error("not found"));
  }
});

module.exports = axios;
