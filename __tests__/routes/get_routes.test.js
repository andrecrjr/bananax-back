const request = require("supertest");
let banController = require("../../build/controller/bananocontroller");

let app, sendBananoToWaxSpy, sendBananoBackSpy;
jest.mock("@bananocoin/bananojs");
let mockBody = {
  created: true,
  bananoAmount: "190",
  rawAmount: "19000000000000000000000000000000",
  error: false,
  sendBack: false,
  info: "190 BANANOS converted to WAX!",
  userReceiver: "11410050971194611997109",
  username: "rd2aw.wam",
  banUserAddress:
    "ban_1hfwr3hwu3yb6pdqu4xgws3jfjbwazjhpwwfe14sz4c9u5wx4nr7excz1589",
};

describe("get server routers", () => {
  beforeEach(async (done) => {
    sendBananoToWaxSpy = jest.spyOn(banController, "convertBananoToWax");
    sendBananoBackSpy = jest.spyOn(banController, "sendBananoBack");
    sendBananoToWaxSpy.mockReturnValue(() => true);
    sendBananoBackSpy.mockImplementation(() => true);

    app = require("../../app");
    done();
  });

  it("should get user address", async (done) => {
    const resp = await request(app).get("/getaddress/rd2aw.wam").expect(200);
    expect(resp.body).toEqual({
      id: "rd2aw.wam",
      banAddress:
        "ban_3zoosfctg9z54kqwwsgx1fu886xczsx1aunqdxbptbzpwbe63tuuxtffg57o",
    });
    done();
  });

  it("should send banano then receive WAX", async (done) => {
    const resp = await request(app).post("/receive/ban-wax").send({
      id: "rd2aw.wam",
      banAddress:
        "ban_1hfwr3hwu3yb6pdqu4xgws3jfjbwazjhpwwfe14sz4c9u5wx4nr7excz1589",
    });
    expect(resp.body).toEqual(mockBody);
    done();
  });

  it("should call sendBananoToWax with data already sent in POST", (done) => {
    expect(sendBananoToWaxSpy).toHaveBeenCalledWith(mockBody);
    done();
  });

  it("should send banano back to user if less than 30 bananos", async (done) => {
    const resp = await request(app).post("/receive/ban-wax").send({
      id: "rd2aw.wam",
      banAddress: "ban_send30bananos",
    });
    expect(resp.body.bananoAmount).toEqual("30");
    expect(resp.body.error).toEqual(true);
    done();
  });
  it("should activate banano back after sent 30 bananos", async (done) => {
    expect(sendBananoBackSpy).toHaveBeenCalledWith({
      created: true,
      bananoAmount: "30",
      rawAmount: "3000000000000000000000000000000",
      error: true,
      sendBack: true,
      info:
        "\n" +
        "            You sent less than minimum to transact... We are giving back your Bananos!",
      userReceiver: "11410050971194611997109",
      username: "rd2aw.wam",
      banUserAddress:
        "ban_1hfwr3hwu3yb6pdqu4xgws3jfjbwazjhpwwfe14sz4c9u5wx4nr7excz1589",
    });
    done();
  });
  afterAll(() => {
    jest.resetAllMocks();
    jest.clearAllMocks();
  });
});
