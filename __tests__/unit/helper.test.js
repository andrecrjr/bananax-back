const { convertBanToWax } = require("../../controller/helper");

jest.mock("axios");

describe("helpers is working", () => {
  it("should get the wax amount back and 5% tax", async () => {
    const data = await convertBanToWax(3000);
    expect(data).toEqual({
      newAmount: "100.69167364197124",
      tax: 5.034583682098562,
    });
  });

  it("should be zero ban then zero wax", async () => {
    const data = await convertBanToWax(0);
    expect(data).toEqual({
      newAmount: "0",
      tax: 0,
    });
  });
});
