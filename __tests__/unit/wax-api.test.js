const { sendWaxAmount } = require("../../server/controller/waxController");
const mockEosTx = (data) => ({
  transaction_id:
    "72542edd0dc3afdfc3770594e8e8a85bd4dd34b49362f93007d89b39404c16f3",
  processed: {
    id: "72542edd0dc3afdfc3770594e8e8a85bd4dd34b49362f93007d89b39404c16f3",
    block_num: 63294801,
    block_time: "2020-12-08T23:38:23.000",
    producer_block_id: null,
    receipt: { status: "executed", cpu_usage_us: 156, net_usage_words: 20 },
    elapsed: 156,
    net_usage: 160,
    scheduled: false,
    action_traces: [
      {
        action_ordinal: 1,
        creator_action_ordinal: 0,
        closest_unnotified_ancestor_action_ordinal: 0,
        receipt: {
          receiver: "eosio.token",
          act_digest:
            "0d78acefd4582060e9149892fce7dba25d972fab0f2d94e2b4ee12a61a5da917",
          global_sequence: 78974919,
          recv_sequence: 812679,
          auth_sequence: [["bananoboatie", 121]],
          code_sequence: 1,
          abi_sequence: 1,
        },
        receiver: "eosio.token",
        act: {
          account: "eosio.token",
          name: "transfer",
          authorization: [{ actor: "bananoboatie", permission: "active" }],
          data: {
            from: `${data.from}`,
            to: `${data.to}`,
            quantity: `${data.eosAmount}`,
            memo: `${data.memo}`,
          },
          hex_data:
            "a05c36f4d069a63980cd34d39c96af4942e1a6060000000008574158000000001f42616e616e617820312e313135393939333820626f756768743a2057415820",
        },
        context_free: false,
        elapsed: 46,
        console: "",
        trx_id:
          "72542edd0dc3afdfc3770594e8e8a85bd4dd34b49362f93007d89b39404c16f3",
        block_num: 63294801,
        block_time: "2020-12-08T23:38:23.000",
        producer_block_id: null,
        account_ram_deltas: [],
        except: null,
        error_code: null,
        inline_traces: [
          {
            action_ordinal: 2,
            creator_action_ordinal: 1,
            closest_unnotified_ancestor_action_ordinal: 1,
            receipt: {
              receiver: "bananoboatie",
              act_digest:
                "0d78acefd4582060e9149892fce7dba25d972fab0f2d94e2b4ee12a61a5da917",
              global_sequence: 78974920,
              recv_sequence: 56,
              auth_sequence: [["bananoboatie", 122]],
              code_sequence: 1,
              abi_sequence: 1,
            },
            receiver: "bananoboatie",
            act: {
              account: "eosio.token",
              name: "transfer",
              authorization: [{ actor: "bananoboatie", permission: "active" }],
              data: {
                from: "bananoboatie",
                to: "darthbananas",
                quantity: "1.11599938 WAX",
                memo: "Bananax 1.11599938 bought: WAX ",
              },
              hex_data:
                "a05c36f4d069a63980cd34d39c96af4942e1a6060000000008574158000000001f42616e616e617820312e313135393939333820626f756768743a2057415820",
            },
            context_free: false,
            elapsed: 3,
            console: "",
            trx_id:
              "72542edd0dc3afdfc3770594e8e8a85bd4dd34b49362f93007d89b39404c16f3",
            block_num: 63294801,
            block_time: "2020-12-08T23:38:23.000",
            producer_block_id: null,
            account_ram_deltas: [],
            except: null,
            error_code: null,
            inline_traces: [],
          },
          {
            action_ordinal: 3,
            creator_action_ordinal: 1,
            closest_unnotified_ancestor_action_ordinal: 1,
            receipt: {
              receiver: "darthbananas",
              act_digest:
                "0d78acefd4582060e9149892fce7dba25d972fab0f2d94e2b4ee12a61a5da917",
              global_sequence: 78974921,
              recv_sequence: 30,
              auth_sequence: [["bananoboatie", 123]],
              code_sequence: 1,
              abi_sequence: 1,
            },
            receiver: "darthbananas",
            act: {
              account: "eosio.token",
              name: "transfer",
              authorization: [{ actor: "bananoboatie", permission: "active" }],
              data: {
                from: "bananoboatie",
                to: "darthbananas",
                quantity: "1.11599938 WAX",
                memo: "Bananax 1.11599938 bought: WAX ",
              },
              hex_data:
                "a05c36f4d069a63980cd34d39c96af4942e1a6060000000008574158000000001f42616e616e617820312e313135393939333820626f756768743a2057415820",
            },
            context_free: false,
            elapsed: 3,
            console: "",
            trx_id:
              "72542edd0dc3afdfc3770594e8e8a85bd4dd34b49362f93007d89b39404c16f3",
            block_num: 63294801,
            block_time: "2020-12-08T23:38:23.000",
            producer_block_id: null,
            account_ram_deltas: [],
            except: null,
            error_code: null,
            inline_traces: [],
          },
        ],
      },
    ],
    account_ram_delta: null,
    except: null,
    error_code: null,
  },
});

jest.mock("axios");
jest.mock("eosjs");
const { Api } = require("eosjs");
let txData;

describe("WAX controller tests", () => {
  beforeAll(async (done) => {
    Api.prototype.transact.mockImplementation(({ actions }) => {
      return new Promise((resolve) =>
        resolve(
          mockEosTx({
            eosAmount: actions[0].data.quantity,
            to: actions[0].data.to,
            from: actions[0].data.from,
            memo: actions[0].data.memo,
          })
        )
      );
    });
    txData = await sendWaxAmount({
      created: true,
      bananoAmount: "35",
      rawAmount: "35000000000000000000000000000000",
      error: false,
      sendBack: false,
      info: "35 BANANOS converted to WAX!",
      userReceiver: "11410050971194611997109",
      username: "rd2aw.wam",
      banUserAddress:
        "ban_1hfwr3hwu3yb6pdqu4xgws3jfjbwazjhpwwfe14sz4c9u5wx4nr7excz1589",
    });
    done();
  });
  it("should receive wax proportional to the bans sent", async () => {
    //banano amount and user to send wax
    expect(txData.processed.action_traces[0].act.data.quantity).toEqual(
      "1.11599938 WAX"
    );
  });
  it("should send WAX to user", async () => {
    expect(txData.processed.action_traces[0].act.data.to).toEqual("rd2aw.wam");
  });
  it("should send WAX from WAX bank", async () => {
    expect(txData.processed.action_traces[0].act.data.from).toEqual(
      "bananoboatie"
    );
  });
});
