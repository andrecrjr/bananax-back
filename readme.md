# Banano > Wax Swap (Bananax)

API Rest to use in a Front-end, using BananoJS from Coranos and eosJS to integrate with WAX Blockchain, of course there can have some flaws in back end architecture...

In summary: this web server at all is just transfering WAX from an account(a bank) to another then you will need an "WAX bank liquidity" to send to the second User's WAX address, you can just create in the tesnet two WAX address and you'll need a main seed, then fill it in the .env.testing.(with the right keys) (using .env.development as model.)

# Working locally

Must have NodeJS then:

> yarn install or npm install

You will need to create an: .env.testing in the root folder using the .env.development as model
and fill with a banano seed, and a wax active key from testnet website: https://waxsweden.org/testnet/
(the end of the page there are inputs to create your wax account on testnet, then you can get it: https://wax-test.bloks.io/ )

> yarn build:linux || yarn build:win

To run this project needs to make the dist/ file from typescript so, you'll need to run the build

> yarn dev

With yarn dev after do the build you will run the JS creted by the Typescript

> yarn test or npm test

Tests are broken because of the typescript merge.

---

# Bugs

There is a mainly bug i havent stopped to think yet, is about the EOS number precision, it has been a stone in shoes, needs to be improved... This bug just happens just when you sent less than 1 WAX in Banano (at least 35 bananos the min bananos), though, it works clearly with 1 wax or more!

---

# API Swagger

- GET: /getaddress/:wax-address - Get user wax address and convert to a index seed and send to the front-end user the Banano Address to the user send his amount.

- POST: /receive/ban-wax - send post with this body:
  `{banAddress: ban_address_i_sent_banano_amount, id:user.waxaddress.wam }`
  we receive his banano amount then this will do all the transactions to the bank AND send to waxConverter.js to send to WAX Testnet

---

You can use EOS JS for your own project and create your own server js based by my waxController, you can read the documentation here about signing transactions with eosjs here: https://developers.eos.io/manuals/eosjs/latest/how-to-guides/how-to-submit-a-transaction

Why EOS JS? Because WAX is just an EOS Based Chain, and works in the same manner, even in its smart contracts, you will need to use eosion.token transact action to send.

thanks for reading!
