import express from "express";
import cors from "cors";

import { waxRoutes, routes } from "./routes";

require("dotenv").config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env",
});

const app = express();
app.use(express.json());
app.use(cors({ credentials: true, origin: true }));

app.use(routes);
app.use(waxRoutes);

export default app;
