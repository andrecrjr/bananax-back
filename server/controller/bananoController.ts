import bananojs from "@bananocoin/bananojs";
import { UserBananoReceive, UserError } from "./bananoTransactions";

import WaxController from "./waxController";

import {
  convertIntoNumber,
  convertBananoAmount,
  convertBanToWax,
} from "./helper";

class BananoController {
  banano;
  constructor() {
    this.banano = bananojs;
    this.banano.setBananodeApiUrl("https://kaliumapi.appditto.com/api");
  }

  async generateAddress(username: string) {
    try {
      const privateKey = await this.banano.getPrivateKey(
        process.env.POTASSIUM_SEED,
        convertIntoNumber(username).toString()
      );
      const publicKey = await this.banano.getPublicKey(privateKey);
      const banAddress = await this.banano.getAccount(publicKey, "ban_");
      return banAddress;
    } catch (e) {
      console.log(e);
    }
  }

  async getUserPendings(
    banAddress: string,
    waxReceiver: string
  ): Promise<UserBananoReceive> {
    /*
  ban Address: we need to receive the user amount then we can get it here
  wax receiver: is the wax user address, we convert it to seed index again
  */
    try {
      const { blocks } = await this.banano.getAccountsPending(
        [banAddress],
        1,
        "true"
      );
      if (Object.keys(blocks[banAddress]).length > 0) {
        const {
          bananoAmount,
          minBananoSent,
          maxBananoSent,
          banUserAddress,
        } = this.getBananoData(blocks, banAddress);
        let waxAmount = await convertBanToWax(bananoAmount);
        return {
          created: true,
          bananoAmount,
          rawAmount: Object.entries(blocks[banAddress])[0][1].amount,
          error: minBananoSent && maxBananoSent ? false : true,
          sendBack: !(minBananoSent && maxBananoSent),
          waxAmount,
          info:
            minBananoSent && maxBananoSent
              ? `${bananoAmount} BANANOS converted to ${(
                  waxAmount.newAmount - waxAmount.tax
                ).toFixed(2)} <strong>WAX</strong> for ${waxReceiver}!`
              : `
            ${
              !minBananoSent
                ? `You sent less than minimum to transact...`
                : `You sent more than we can transact by now...`
            } We are giving back your Bananos!`,
          waxReceiver: convertIntoNumber(waxReceiver),
          username: waxReceiver,
          banUserAddress,
        };
      }
      return {
        created: false,
        error: true,
        sendBack: false,
        waxReceiver,
        info: "No pending transactions found, did you send some Bananos?",
        waxAmount: {
          newAmount: 0,
          tax: 0,
        },
      };
    } catch (error) {
      console.log(error);
      return {
        created: false,
        error: true,
        waxReceiver,
        sendBack: true,
        info: "Problem with the API connection, contact with the owner Eroshi",
        waxAmount: {
          newAmount: 0,
          tax: 0,
        },
      };
    }
  }

  getBananoData(
    blocks: bananojs.ITransactionBlocks,
    banAddress: string
  ): {
    bananoAmount: number;
    maxBananoSent: boolean;
    minBananoSent: boolean;
    banUserAddress: string;
  } {
    const bananoAmount = convertBananoAmount(
      Object.entries(blocks[banAddress])[0][1].amount
    );
    const banUserAddress = Object.entries(blocks[banAddress])[0][1].source;
    const minBananoSent =
      parseInt(bananoAmount) >= parseFloat(process.env.MIN_BAN_SENT);
    const maxBananoSent =
      parseFloat(process.env.MAX_BAN_SENT) >= parseInt(bananoAmount);
    return { bananoAmount, maxBananoSent, minBananoSent, banUserAddress };
  }

  async convertBananoToWax(data: UserBananoReceive) {
    try {
      if (data.created) {
        await this.banano.receiveBananoDepositsForSeed(
          process.env.POTASSIUM_SEED,
          data.waxReceiver,
          "ban_1sebrep1mbkdtdb39nsouw5wkkk6o497wyrxtdp71sm878fxzo1kwbf9k79b"
        );
        const sent = await this.sendBananosToBank(
          data.rawAmount,
          data.waxReceiver
        );

        if (sent) {
          const GetWax = new WaxController();
          await GetWax.sendWaxAmount(data);
          process.env.NODE_ENV === "test" &&
            (await this.sendBananoBack(data, true));
          return {
            created: true,
            error: false,
            info: `Your amount of ${data.bananoAmount} BANANOs was received 
            and will send your WAX to your account asap!`,
          };
        }
      }
    } catch (error) {
      console.log(error);
      return {
        created: false,
        error: true,
        info: "Problem with the transaction please contact the admin: Eroshi",
      };
    }
  }

  async sendBananosToBank(rawAmount: string, userIndex: string) {
    try {
      await this.banano.sendAmountToBananoAccountWithRepresentativeAndPrevious(
        process.env.POTASSIUM_SEED,
        userIndex,
        process.env.BAN_BANK,
        rawAmount,
        "ban_1sebrep1mbkdtdb39nsouw5wkkk6o497wyrxtdp71sm878fxzo1kwbf9k79b"
      );
      await this.banano.receiveBananoDepositsForSeed(
        process.env.POTASSIUM_SEED,
        "0",
        "ban_1sebrep1mbkdtdb39nsouw5wkkk6o497wyrxtdp71sm878fxzo1kwbf9k79b"
      );
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async sendBananoBack(data: UserBananoReceive, toBank = false) {
    try {
      const transact = await this.banano.receiveBananoDepositsForSeed(
        process.env.POTASSIUM_SEED,
        data.waxReceiver,
        "ban_1sebrep1mbkdtdb39nsouw5wkkk6o497wyrxtdp71sm878fxzo1kwbf9k79b"
      );
      if (transact)
        await this.banano.sendAmountToBananoAccountWithRepresentativeAndPrevious(
          process.env.POTASSIUM_SEED,
          toBank ? "0" : data.waxReceiver,
          data.banUserAddress,
          data.rawAmount,
          "ban_1sebrep1mbkdtdb39nsouw5wkkk6o497wyrxtdp71sm878fxzo1kwbf9k79b"
        );
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
}

export default BananoController;
