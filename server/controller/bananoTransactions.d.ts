export interface UserBananoReceive {
  created?: boolean;
  bananoAmount?: any;
  rawAmount?: any;
  error: boolean;
  sendBack: boolean;
  info: string;
  waxAmount: {
    newAmount: number;
    tax: number;
  };
  waxReceiver: string;
  username?: string;
  banUserAddress?: any;
}

export interface UserError {
  created: boolean;
  sendBack: boolean;
  error: true;
  info: "Problem with the API connection, contact with the owner Eroshi";
}
