declare module "@bananocoin/bananojs" {
  type accountPrefix = "ban_" | "nano_";

  interface ITransactionBlocks {
    [key: string]: {
      [key: string]: {
        amount: string;
        source: string;
      };
    };
  }

  type Blocks = Record<"blocks", ITransactionBlocks>;

  function setBananodeApiUrl(seed: string): string;
  function getPrivateKey(seed: string | undefined, index: string): string;
  function getPublicKey(privateKey: string): Promise<string>;
  function getAccount(
    publicKey: string,
    accountPrefix: accountPrefix
  ): Promise<string>;
  function getAccountsPending(
    account: string[],
    count?: number,
    source?: string
  ): Promise<Blocks>;
  function receiveBananoDepositsForSeed(
    seed: string,
    seedIx: string,
    rep?: string,
    specificPendingBlockHash?: string
  ): Promise<{}>;

  function sendAmountToBananoAccountWithRepresentativeAndPrevious(
    seed: string,
    seedIx: string,
    destAccount: string,
    amountRaw: string,
    rep?: string,
    prevHash?: string
  ): Promise<string>;
}
