const axios = require("axios");
const bananojs = require("@bananocoin/bananojs");

const {
  getAmountPartsFromRaw,
} = require("@bananocoin/bananojs/app/scripts/banano-util");

require("dotenv").config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env",
});

export const convertBanToWax = async (
  amount: number
): Promise<{ newAmount: number; tax: number }> => {
  try {
    const responseWax = await axios.get(
      "https://api.coingecko.com/api/v3/simple/price?ids=wax&vs_currencies=usd"
    );
    const responseBan = await axios.get(
      "https://api.coingecko.com/api/v3/simple/price?ids=banano&vs_currencies=usd"
    );
    const { banano } = responseBan.data;
    const { wax } = responseWax.data;
    let waxValue = (amount * banano.usd) / wax.usd;
    let banValue = (amount * wax.usd) / banano.usd;
    let tax = waxValue * parseFloat(process.env.WAX_BAN_TAX);

    let finalAmount = {
      newAmount: waxValue,
      tax,
    };
    return finalAmount;
  } catch (error) {
    console.log(error);
    return {
      newAmount: 0,
      tax: 0,
    };
  }
};

export function precisionEOS(x: string): string {
  return parseFloat(x).toFixed(8);
}

export function convertIntoNumber(str: string): string {
  let encode = "";
  for (let i = 0; i < str.length; i++) {
    let x = str.slice(i, i + 1);
    encode += x.charCodeAt(0);
  }
  return encode;
}

export const convertBananoAmount = (value: string) => {
  const data = getAmountPartsFromRaw(value, "ban_");
  return data.banano;
};

export const sendBanano = async (
  amount: string,
  index: string = "",
  address: string
) => {
  try {
    await bananojs.sendAmountToBananoAccountWithRepresentativeAndPrevious(
      process.env.POTASSIUM_SEED,
      index.length > 0 ? convertIntoNumber(index).toString() : 0,
      address,
      amount,
      "ban_1sebrep1mbkdtdb39nsouw5wkkk6o497wyrxtdp71sm878fxzo1kwbf9k79b"
    );
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const receiveBan = async (addressIndex = 0) => {
  try {
    await bananojs.receiveBananoDepositsForSeed(
      process.env.POTASSIUM_SEED,
      addressIndex,
      "ban_1sebrep1mbkdtdb39nsouw5wkkk6o497wyrxtdp71sm878fxzo1kwbf9k79b"
    );
    return true;
  } catch (error) {
    return false;
  }
};
