import { Api, JsonRpc } from "eosjs";
import { JsSignatureProvider } from "eosjs/dist/eosjs-jssig";
import { TextDecoder, TextEncoder } from "util"; //node only
import fetch from "node-fetch";
import { precisionEOS } from "./helper";

require("dotenv").config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env",
});

import { UserBananoReceive } from "./bananoTransactions";

let privateKey1 = process.env.WAX_KEY;

const privateKeys = [privateKey1];

const signatureProvider = new JsSignatureProvider(privateKeys);
class WaxController {
  api: Api;
  rpc: JsonRpc;
  constructor() {
    this.rpc = new JsonRpc(process.env.JSON_RPC_WAX, { fetch });
    this.api = new Api({
      rpc: this.rpc,
      signatureProvider,
      textEncoder: new TextEncoder(),
    });
  }

  sendWaxAmount = async (data: UserBananoReceive) => {
    try {
      let getTaxOut: string = String(
        data.waxAmount.newAmount - data.waxAmount.tax
      );
      const transaction = await this.api.transact(
        {
          actions: [
            {
              account: "eosio.token",
              name: "transfer",
              authorization: [
                {
                  actor:
                    process.env.NODE_ENV === "test"
                      ? `bananoboatie`
                      : process.env.WAX_BANK_USER,
                  permission: "active",
                },
              ],
              data: {
                from:
                  process.env.NODE_ENV === "test"
                    ? `bananoboatie`
                    : process.env.WAX_BANK_USER,
                to: data.username,
                quantity: `${precisionEOS(getTaxOut)} WAX`,
                memo: `sent ${
                  data.bananoAmount
                } BANANO(S) and now you receive ${precisionEOS(
                  getTaxOut
                )} WAX by Bananax`,
              },
            },
          ],
        },
        {
          blocksBehind: 3,
          expireSeconds: 30,
        }
      );
      return transaction;
    } catch (e) {
      console.log(e);
      console.log(e.json.error.details);
      return false;
    }
  };
}

export default WaxController;
