declare namespace NodeJS {
  export interface ProcessEnv {
    POTASSIUM_SEED: string;
    WAX_KEY: string;
    JSON_RPC_WAX: string;
    MIN_BAN_SENT: string;
    MAX_BAN_SENT: string;
    BAN_BANK: string;
    WAX_BANK: string;
    WAX_BAN_TAX: string;
  }
}
