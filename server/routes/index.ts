import { Request, Response, Router } from "express";

import BananoController from "../controller/bananoController";

const routes = Router();
const waxRoutes = Router();

routes.get("/healthcheck", (res: Response) => {
  return res.status(200).send("healthcheck");
});

waxRoutes.get("/getaddress/:address", async (req: Request, res: Response) => {
  const newAddress = new BananoController();
  return res.json({
    id: req.params.address,
    banAddress: await newAddress.generateAddress(req.params.address),
  });
});

waxRoutes.get("/price/wax-ban", async (req: Request, res: Response) => {
  res.send(parseFloat(req.body.amount).toFixed(8));
});

waxRoutes.post("/receive/ban-wax", async (req: Request, res: Response) => {
  try {
    const ReceiveBanController = new BananoController();
    const data = await ReceiveBanController.getUserPendings(
      req.body.banAddress,
      req.body.id
    );
    if (data["created"]) {
      res.status(200).json(data);
      data["sendBack"]
        ? await ReceiveBanController.sendBananoBack(data)
        : await ReceiveBanController.convertBananoToWax(data);
    } else {
      res.status(404).json(data);
    }
    return;
  } catch (error) {
    console.log(error);
  }
});

export { waxRoutes, routes };
